function showTimeLeftToday() {
    let now = new Date();
    let tomorrow = new Date(now.getFullYear(), now.getMonth(), now.getDate()+1);
    let diff = tomorrow - now;
    let timeleft = Math.round(diff / 100);

    let fraction = timeleft.toString().slice(-1);
    let seconds = timeleft.toString().substr(0, timeleft.toString().length - 1)

    document.getElementById('seconds').innerHTML = seconds;
    document.getElementById('fraction').innerHTML = "." + fraction;
}

showTimeLeftToday();
window.addEventListener("load", function() {
    timer = setInterval(showTimeLeftToday, 100);
});
